﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace TESTApplication
{
    [Activity(Theme = "@style/Splash",Label = "@string/app_name", MainLauncher = true,   ScreenOrientation = ScreenOrientation.Portrait)]
    public class ActivityBoot : AppCompatActivity
    {


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Check_Permissions();

        }

        public void Check_Permissions()
        {
            var requiredPermissions = new String[] {
               Android.Manifest.Permission.ReadExternalStorage,
               Android.Manifest.Permission.WriteExternalStorage,
               Android.Manifest.Permission.ReadPhoneState,
               Android.Manifest.Permission.AccessNotificationPolicy,
               Android.Manifest.Permission.CallPhone,
               Android.Manifest.Permission.Vibrate,
               Android.Manifest.Permission.ReadContacts,
               Android.Manifest.Permission.WriteContacts,
               Android.Manifest.Permission.ReceiveBootCompleted
                //claudio
                ,
                //Manifest.Permission.ReadSms,
                //Manifest.Permission.ReceiveSms ,
                 Android.Manifest.Permission.ReadCallLog 
                //claudio
               };
            Android.Support.V4.App.ActivityCompat.RequestPermissions(this, requiredPermissions, 1);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            if (requestCode == 1)
            {
                // Scrittura

                bool ok = true;
                if (grantResults[0] == Permission.Granted)
                {

                }
                else
                {
                    ok = false;
                }

                // Lettura
                if (grantResults[1] == Permission.Granted)
                {

                }
                else
                {
                    ok = false;
                }
                // Stato Telefono
                //if (grantResults[2] == Permission.Granted)
                //{

                //}
                //else
                //{
                //    ok = false;
                //}
                //if (grantResults[3] == Permission.Granted)
                //{

                //}
                //else
                //{
                //    ok = false;
                //}
                if (grantResults[4] == Permission.Granted)
                {

                }
                else
                {
                    ok = false;
                }
                if (grantResults[5] == Permission.Granted)
                {

                }
                else
                {
                    ok = false;
                }
                if (grantResults[6] == Permission.Granted)
                {

                }
                else
                {
                    ok = false;
                }
                //if (grantResults[7] == Permission.Granted)
                //{

                //}
                //else
                //{
                //    ok = false;
                //}
                if (grantResults[8] == Permission.Granted)
                {

                }
                else
                {
                    ok = false;
                } //claudio
                if (grantResults[9] == Permission.Granted)
                {

                }
                else
                {
                    ok = false;
                }

                if (ok)
                {
                    StartActivity(typeof(StartActivity));

                }
                else
                {
                    Check_Permissions();
                }

            }

            else
            {
                base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            }


        }
    }
}