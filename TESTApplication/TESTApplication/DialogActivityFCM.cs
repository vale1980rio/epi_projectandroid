﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Refractored.Controls;
using static Android.App.ActivityManager;


namespace TESTApplication
{

    [Activity(Theme = "@style/AppTheme", Label = "Allarme", NoHistory = true, LaunchMode = Android.Content.PM.LaunchMode.SingleInstance, ScreenOrientation = ScreenOrientation.Portrait)]

    public class DialogActivityFCM : Activity
    {

        private BroadcastMessageCatcherFCM mReceiver;

        Vibrator vibrator = (Vibrator)Application.Context.GetSystemService(Context.VibratorService);

        Button btn;
        Dialog dialog;
        MediaPlayer mPlayer;

        string titolo = string.Empty;
        string messaggio = string.Empty;
        string topic_msg = string.Empty;

        public void KillApplication()
        {
            this.FinishAndRemoveTask();

        }


        protected override void OnDestroy()
        {

            base.OnDestroy();
            StopPlayer();
        }


        protected override void OnResume()
        {
            base.OnResume();

            IntentFilter intentFilter = new IntentFilter("android.intent.action.DIALOG_FCM");

            mReceiver = new BroadcastMessageCatcherFCM();
            mReceiver.setDialogActivityHandler(this);
            this.RegisterReceiver(mReceiver, intentFilter);
        }


        protected override void OnPause()
        {
            base.OnPause();
            this.UnregisterReceiver(mReceiver);
        }


        protected void ok_click(object sender, EventArgs e)
        {


            KillApplication();
        }

        protected void no_click(object sender, EventArgs e)
        {
            KillApplication();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);




            SetContentView(Resource.Layout.Alarm);
            LinearLayout layout = (LinearLayout)FindViewById(Resource.Id.linearLayout1);
            AnimationDrawable drawable = new AnimationDrawable();
            Handler handler = new Handler();

            drawable.AddFrame(new ColorDrawable(Color.White), 300);
            drawable.AddFrame(new ColorDrawable(Color.ParseColor("#7c2a2a")), 300);
            drawable.OneShot = false;



            try
            {
#pragma warning disable CS0618 // Il tipo o il membro è obsoleto
                layout.SetBackgroundDrawable(drawable);
#pragma warning restore CS0618 // Il tipo o il membro è obsoleto
            }
            catch
            {

            }

            Handler h = new Handler();
            Action myAction = () =>
            {
                drawable.Start();
            };

            h.PostDelayed(myAction, 1000);



            titolo = Intent.GetStringExtra("titolo") ?? string.Empty;
            messaggio = Intent.GetStringExtra("messaggio") ?? string.Empty;
            topic_msg = Intent.GetStringExtra("topic") ?? string.Empty;

            dialog = new Dialog(this);
            dialog.Window.SetDimAmount(0);
            dialog.RequestWindowFeature((int)WindowFeatures.NoTitle); //before 
            dialog.SetContentView(Resource.Layout.Dialog_Header_Custom_FCM);
            dialog.SetTitle("Custom Alert Dialog");
            dialog.SetCancelable(false);


            Window.SetGravity(GravityFlags.Top);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn |
               WindowManagerFlags.DismissKeyguard |
               WindowManagerFlags.ShowWhenLocked |
               WindowManagerFlags.TurnScreenOn);



            ImageButton btnClose = (ImageButton)dialog.FindViewById(Resource.Id.close);

            btnClose.Click += no_click;

            TextView utente = (TextView)dialog.FindViewById(Resource.Id.editText);
            TextView numero = (TextView)dialog.FindViewById(Resource.Id.editTextNumber);
            TextView topic = (TextView)dialog.FindViewById(Resource.Id.topic);

            CircleImageView image = (CircleImageView)dialog.FindViewById(Resource.Id.imageViewContact);
            image.SetImageResource(Resource.Drawable.AlarmImage);

            utente.Text = titolo.ToUpper();
            numero.Text = messaggio;
            topic.Text = "EPI - Canale: " + topic_msg;
            // btnClose.Click += no_click;

            dialog.Show();

            StartPlayer();
        }



        public void StopPlayer()
        {
            mPlayer.Stop();
            vibrator.Cancel();
        }

        public void ChangeText(string testo)
        {
            TextView messageView = (TextView)dialog.FindViewById(Android.Resource.Id.Message);
            messageView.Text = testo;
        }

        public void RefreshText(string titolo, string messaggio, string text_topic)
        {
            TextView topic = (TextView)dialog.FindViewById(Resource.Id.topic);
            TextView utente = (TextView)dialog.FindViewById(Resource.Id.editText);
            TextView numero = (TextView)dialog.FindViewById(Resource.Id.editTextNumber);

            utente.Text = titolo.ToUpper();
            numero.Text = messaggio;
            topic.Text = text_topic;
            int maxVol = am.GetStreamMaxVolume(Android.Media.Stream.Music);



            // int maxVol = 2;
            am.SetStreamVolume(Android.Media.Stream.Music, maxVol, VolumeNotificationFlags.PlaySound);

        }
        AudioManager am = (AudioManager)Android.App.Application.Context.GetSystemService(Context.AudioService);


        public void StartPlayer()
        {
            if (mPlayer == null)
            {
                mPlayer = new MediaPlayer();
            }


            int maxVol = am.GetStreamMaxVolume(Android.Media.Stream.Music);



            //int maxVol = 2;

            am.SetStreamVolume(Android.Media.Stream.Music, maxVol, VolumeNotificationFlags.PlaySound);

            //NotificationManager mNotificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            //if (mNotificationManager.IsNotificationPolicyAccessGranted)
            //{
            //    am.RingerMode = RingerMode.Normal;
            //}
            //mPlayer.Reset();
            var fd = global::Android.App.Application.Context.Assets.OpenFd("tornado.mp3");
            mPlayer.SetDataSource(fd.FileDescriptor, fd.StartOffset, fd.Length);
            //mPlayer.SetDataSource(filePath);
            mPlayer.Prepare();
            mPlayer.Looping = true;
            mPlayer.Start();
            long[] mVibratePattern = new long[] { 0, 1000, 200, 1000, 400, 1000, 600, 1000, 800, 1000, 800, 1000, 600, 1000, 400, 1000, 200, 1000 };

            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                vibrator.Vibrate(VibrationEffect.CreateWaveform(mVibratePattern, 0));
            }
            else
            {
                try
                {
#pragma warning disable CS0618 // Il tipo o il membro è obsoleto
                    vibrator.Vibrate(mVibratePattern, 0);
#pragma warning restore CS0618 // Il tipo o il membro è obsoleto
                }
                catch
                {

                }
            }

        }


    }

    [BroadcastReceiver]
    [IntentFilter(new[] { "android.intent.action.DIALOG_FCM" })]
    public class BroadcastMessageCatcherFCM : BroadcastReceiver
    {
        DialogActivityFCM da = null;

        public void setDialogActivityHandler(DialogActivityFCM main)
        {
            da = main;
        }


        public override void OnReceive(Context context, Intent intent)
        {
            if (da != null)
            {
                string canale = intent.GetStringExtra("canale");
                string messaggio = intent.GetStringExtra("titolo");
                string titolo = intent.GetStringExtra("messaggio");

                da.RefreshText(messaggio, titolo,canale);
            }
        }
    }
}