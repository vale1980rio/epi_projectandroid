﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using TESTApplication.Utility;

namespace TESTApplication.Fragments
{
    public class Fragment_Canali : Fragment
    {

        Sqlite sqlite = new Sqlite();

        CustomAdapterFCM customFCM;
        List<Alert_Messages> alert_messages;
        ListView listview_messages;
        ImageButton floatingActionButton;
        View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            // Use this to return your custom view for this Fragment
            view = inflater.Inflate(Resource.Layout.MainFCM, container, false);

            #region Drawer
            listview_messages = view.FindViewById<ListView>(Resource.Id.ListView);

            floatingActionButton = view.FindViewById<ImageButton>(Resource.Id.fab);
            floatingActionButton.Click += Btn_ClicCercaContatto;


            #endregion

            //return base.OnCreateView(inflater, container, savedInstanceState);

            return view;
        }

        public override void OnResume()
        {
            base.OnResume();
            RefreshGrid();

        }
        private void Btn_ClicCercaContatto(object sender, EventArgs e)
        {
            sqlite.Delete_Messages();
            RefreshGrid();
        }
        private void RefreshGrid()
        {
            Sqlite sqlite = new Sqlite();
            alert_messages = new List<Alert_Messages>();
            alert_messages = sqlite.Get_Messages();
            // populate the listview with data

            customFCM = new CustomAdapterFCM(this.Activity, alert_messages);
            listview_messages.Adapter = customFCM;

            ListView listViewFriends = (ListView)view.FindViewById(Resource.Id.ListView);
            // set your adapter here
            // set your click listener here
            // or whatever else
            listViewFriends.EmptyView = view.FindViewById(Resource.Id.empty);
        }
    }
}