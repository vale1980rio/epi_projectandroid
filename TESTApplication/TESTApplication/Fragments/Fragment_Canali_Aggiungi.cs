﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Firebase.Messaging;
using Refractored.Controls;
using TESTApplication.Utility;

namespace TESTApplication.Fragments
{
    public class Fragment_Canali_Aggiungi : Fragment
    {
        CustomAdapterEDIFICI customEDIFICI;
        List<string> edifici;
        ListView listview_edifici;
        Sqlite sqlite = new Sqlite();
        View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            view = inflater.Inflate(Resource.Layout.MainEDIFICI, container, false);


            #region Drawer
            listview_edifici = view.FindViewById<ListView>(Resource.Id.ListView);

           // listview_edifici.ItemClick += Contacts_ItemClick;

            #endregion

            return view;
        }
        public View getViewByPosition(int pos, ListView listView)
        {
            int firstListItemPosition = listView.FirstVisiblePosition;
            int lastListItemPosition = firstListItemPosition + listView.ChildCount - 1;

            if (pos < firstListItemPosition || pos > lastListItemPosition)
            {
                return listView.Adapter.GetView(pos, null, listView);
            }
            else
            {
                int childIndex = pos - firstListItemPosition;
                return listView.GetChildAt(childIndex);
            }
        }
        private void Contacts_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ListView list = (ListView)sender;
            var item = list.GetItemAtPosition(e.Position);
            View v = getViewByPosition(e.Position, list);

            var text_1 = v.FindViewById<TextView>(Resource.Id.Description).Text;

           

           // string test = ObjectTypeHelper.Cast<string>(item);

           
            FirebaseMessaging.Instance.UnsubscribeFromTopic("ROMA_EUR");
            FirebaseMessaging.Instance.UnsubscribeFromTopic("GENERALE");
            FirebaseMessaging.Instance.UnsubscribeFromTopic("PROTEZIONE_CIVILE");

            FirebaseMessaging.Instance.SubscribeToTopic(text_1.Replace(" ","_"));

            ToastColor.PosteToastColor("Sottoscrizione ad Edificio " + text_1 + " effettuata con successo. Tutte le altre sottoscrizioni sono state eliminate.", this.Activity);

            sqlite.Insert_Edificio(text_1);
        }

        public override void OnResume()
        {
            base.OnResume();
            RefreshGrid();

        }
        private void RefreshGrid()
        {
            Sqlite sqlite = new Sqlite();
            edifici = new List<string>();

            edifici.Add("ROMA EUR");
            edifici.Add("GENERALE");
            edifici.Add("PROTEZIONE CIVILE");
            
            // populate the listview with data

            customEDIFICI = new CustomAdapterEDIFICI(this.Activity, edifici);
            listview_edifici.Adapter = customEDIFICI;

            ListView listViewFriends = (ListView)view.FindViewById(Resource.Id.ListView);
            // set your adapter here
            // set your click listener here
            // or whatever else
            listViewFriends.EmptyView = view.FindViewById(Resource.Id.empty);
        }
    }
}