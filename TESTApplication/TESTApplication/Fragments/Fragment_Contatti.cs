﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Refractored.Controls;
using TESTApplication.Utility;
using static Android.Provider.ContactsContract.CommonDataKinds;

namespace TESTApplication.Fragments
{
    public class Fragment_Contatti : Fragment
    {
        public bool write = false;
        string uri_str = "";
        //View layout;
        CustomListAdapter custom;
        ImageButton floatingActionButton;

        List<PriorityNumbers> priorityNumbers;
        ListView contacts;
        View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }
        

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            Sqlite sqlite = new Sqlite();
            sqlite.Delete_PNumbers(contact_to_delete);
            Get_Number();
            dialog.Dismiss();
        }

        Dialog dialog;

        string contact_to_delete = "-1";


        private void listView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {
            dialog = new Dialog(this.Activity);
            dialog.RequestWindowFeature((int)WindowFeatures.NoTitle); //before  
            dialog.SetContentView(Resource.Layout.custom_dialog);
            ListView list = (ListView)sender;
            var item = list.GetItemAtPosition(e.Position);
            PriorityNumbers test = ObjectTypeHelper.Cast<PriorityNumbers>(item);


            ImageButton btnDelete = (ImageButton)dialog.FindViewById(Resource.Id.save);


            CircleImageView image = (CircleImageView)dialog.FindViewById(Resource.Id.imageViewContact);
            TextView utente = (TextView)dialog.FindViewById(Resource.Id.editText);


            image.SetImageURI(Android.Net.Uri.Parse(test.photo_uri));

            if (image.Drawable == null)
            {
                image.SetImageResource(Resource.Drawable.ContactImage);
            }

            utente.Text = test.Descrizione;
            btnDelete.Click += BtnDelete_Click;
            contact_to_delete = test.Contact_ID;

            dialog.Show();



        }


        [Obsolete]
        public override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case (17):

                    string numero = "";
                    int count_numeri = 0;
                    if (resultCode == Result.Ok)
                    {

                        Android.Net.Uri contactData = data.Data;
                        ContentResolver resolver = Activity.ContentResolver;
                        var c = resolver.Query(contactData, null, null, null, null);
                        if (c.MoveToFirst())
                        {

                            String contactId = c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.Id));
                            long contactId_long = long.Parse(c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.Id)));

                            var contactUri = ContentUris.WithAppendedId(ContactsContract.Contacts.ContentUri, contactId_long);
                            var contactPhotoUri = Android.Net.Uri.WithAppendedPath(contactUri, Contacts.Photos.ContentDirectory);

                            var uri = ContactsContract.Contacts.ContentUri;
                            uri_str = contactPhotoUri.ToString();
                            string[] projection = { Phone.Number, CommonColumns.Type };

                            
                            var cursor = resolver.Query(Phone.ContentUri, projection, ContactsContract.RawContactsColumns.ContactId + "=" + contactId, null, null);
                            count_numeri = cursor.Count;
                            if (cursor != null)
                            {
                                if (count_numeri > 0)
                                {
                                    while (cursor.MoveToNext())
                                    {
                                        Sqlite sqlite = new Sqlite();
                                        string num = cursor.GetString(0);
                                        string result = string.Concat(num.Where(rr => !char.IsWhiteSpace(rr)));

                                        sqlite.Insert_PNumbers(result, uri_str, c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.DisplayName)), c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.Id)), 2);
                                    }
                                }
                            }
                            c.Close();
                            cursor.Close();
                        }
                    }
                    Get_Number();
                    break;
            }

        }
        private void Contacts_LongClick(object sender, View.LongClickEventArgs e)
        {

        }

        public View getViewByPosition(int pos, ListView listView)
        {
            int firstListItemPosition = listView.FirstVisiblePosition;
            int lastListItemPosition = firstListItemPosition + listView.ChildCount - 1;

            if (pos < firstListItemPosition || pos > lastListItemPosition)
            {
                return listView.Adapter.GetView(pos, null, listView);
            }
            else
            {
                int childIndex = pos - firstListItemPosition;
                return listView.GetChildAt(childIndex);
            }
        }

        private void Contacts_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ListView list = (ListView)sender;
            var item = list.GetItemAtPosition(e.Position);
            View v = getViewByPosition(e.Position, list);

            var text_1 = v.FindViewById<TextView>(Resource.Id.Title).Text;

            PriorityNumbers test = ObjectTypeHelper.Cast<PriorityNumbers>(item);
            Sqlite sqlite = new Sqlite();
            sqlite.Insert_VIC_Level(test.Contact_ID, VicConverter.Next_Vic_Code(test.VicLevel));
            //

            test.VicLevel = VicConverter.Next_Vic_Code(test.VicLevel);


            if (v == null)
                return;

            VicConverter.VicItemListViewConverter(v, test);

        }

        private void Btn_ClickInserisciContatto(object sender, EventArgs e)
        {
            Intent intent = new Intent(Intent.ActionInsert);
            intent.SetType(ContactsContract.Contacts.ContentType);
            StartActivityForResult(intent, 18);
        }
        private void Btn_ClicCercaContatto(object sender, EventArgs e)
        {
            Intent intent = new Intent(Intent.ActionPick);
            intent.SetData(ContactsContract.Contacts.ContentUri);
            StartActivityForResult(intent, 17);
        }
        public override void OnResume()
        {
            base.OnResume();
            if (write)
                Get_Number();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            view = inflater.Inflate(Resource.Layout.Main, container, false);
            floatingActionButton = view.FindViewById<ImageButton>(Resource.Id.fab);
            floatingActionButton.Click += Btn_ClicCercaContatto;

            contacts = view.FindViewById<ListView>(Resource.Id.ListView);
            contacts.ItemClick += Contacts_ItemClick;
            contacts.ItemLongClick += listView_ItemLongClick;

            Get_Number();
            return view;
        }
        private bool ValidatePhoneNumber(string phone)
        {
            return Android.Util.Patterns.Phone.Matcher(phone).Matches();
        }
        public  void Get_Number()
        {
            RefreshGrid();
        }

        public void RefreshGrid()
        {

            Sqlite sqlite = new Sqlite();
            priorityNumbers = new List<PriorityNumbers>();
            priorityNumbers = sqlite.Get_Contacts();
            // populate the listview with data

            custom = new CustomListAdapter(this.Activity, priorityNumbers);
            contacts.Adapter = custom;

            ListView listViewFriends = (ListView)view.FindViewById(Resource.Id.ListView);
            // set your adapter here
            // set your click listener here
            // or whatever else
            listViewFriends.EmptyView = view.FindViewById(Resource.Id.empty);
        }
    }


    public static class ObjectTypeHelper
    {
        public static T Cast<T>(this Java.Lang.Object obj) where T : class
        {
            var propertyInfo = obj.GetType().GetProperty("Instance");
            return propertyInfo == null ? null : propertyInfo.GetValue(obj, null) as T;
        }
    }
}