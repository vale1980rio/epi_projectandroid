﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TESTApplication.PersonalDataQueries;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using Android.Support.Design.Widget;
using Android.Graphics;
using Android.Text;
using Android.Text.Style;
using Android.Support.V4.App;
using Android;
using Android.Content.PM;
using TESTApplication.Utility;
using static Android.App.ActionBar;

namespace TESTApplication
{


    [BroadcastReceiver()]
    [IntentFilter(new[] { "android.intent.action.PHONE_STATE" })]
    public class IncomingCallBroadCasteceiver : BroadcastReceiver
    {
        Sqlite sqlite = new Sqlite();
       
        string TelephoneNumber;
        int minuti = 5;
        int chiamate = 5;

        public override void OnReceive(Context context, Intent intent)
        {
           

            TelephonyManager mTel = (TelephonyManager)context.GetSystemService("phone");
            TelephoneNumber = intent.GetStringExtra("incoming_number");
            sqlite.Cancella_Chiamate_Oltre_1_Giorno();


            if (TelephoneNumber != null)
            {


                List<CountryCode> codes = CountryCodes.GetAllCodes();

                foreach (var code in codes)
                {
                    if (TelephoneNumber.Contains(code.Code))
                    {
                        TelephoneNumber = TelephoneNumber.Replace(code.Code, "").Trim();
                        break;
                    }
                }

                if (sqlite.Get_Number_Count(TelephoneNumber) > 0)
                {
                    var intent_msg  = new Intent(context, typeof(StartActivity));

                    //UCCIDE IL POPUP PRECEDENTE SE ATTIVO.
                    Intent i = new Intent("android.intent.action.DIALOG_BC");
                    context.SendBroadcast(i);


                    switch (mTel.CallState)
                    {
                        case CallState.Idle:
                            {
                                if (ActivityCompat.CheckSelfPermission(context, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
                                {
                                    ToastColor.YellowToastColor("V.I.P. Nessun diritto di accesso memoria in scrittura.", context);
                                }
                                else
                                {
                                    if (sqlite.Get_Number_Count(TelephoneNumber) > 0)
                                        if (sqlite.Get_VicLevel(TelephoneNumber) > 0)
                                            Idle(context);
                                }


                                break;
                            }
                        case CallState.Offhook:
                            {
                                if (ActivityCompat.CheckSelfPermission(context, Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
                                {
                                    ToastColor.YellowToastColor("V.I.P. Nessun diritto di accesso memoria in scrittura.", context);
                                }
                                else
                                {
                                    if (sqlite.Get_Number_Count(TelephoneNumber) > 0)
                                        if (sqlite.Get_VicLevel(TelephoneNumber) > 0)
                                            sqlite.Insert_Numbers_States(TelephoneNumber, (int)CallState.Offhook, "Offhook");
                                }
                                break;
                            }
                        case CallState.Ringing:
                            {
                                if (ActivityCompat.CheckSelfPermission(context, Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
                                {
                                    ToastColor.YellowToastColor("V.I.P. Nessun diritto di accesso memoria in scrittura.", context);
                                }
                                else
                                {
                                    if (sqlite.Get_Number_Count(TelephoneNumber) > 0)
                                        if (sqlite.Get_VicLevel(TelephoneNumber) > 0)
                                            sqlite.Insert_Numbers_States(TelephoneNumber, (int)CallState.Ringing, "Ringing");
                                }

                                break;
                            }
                    }
                }
            }
        }

        private void Idle(Context context)
        {

            Sqlite sqlite = new Sqlite();
            int vic_level = sqlite.Get_VicLevel(TelephoneNumber);
            string nome = sqlite.Get_Descrizione(TelephoneNumber);
            chiamate = VicConverter.VIC_Chiamate_Converter(vic_level);


            PersonalInformations alarm = new PersonalInformations(minuti, chiamate, TelephoneNumber);
            if (alarm.CHECK_STATUS_MISSED_CALLS())
            {
                Intent aInt1 = new Intent();
                aInt1.AddFlags(ActivityFlags.SingleTop);
                aInt1.AddFlags(ActivityFlags.NewTask);
                aInt1.AddFlags(ActivityFlags.NoHistory);
                    aInt1.SetClass(Android.App.Application.Context, typeof(DialogActivity));

                aInt1.PutExtra("number", TelephoneNumber);
                aInt1.PutExtra("chiamate", alarm.missed_calls.ToString());
                aInt1.PutExtra("photoURI", sqlite.Get_PhotoUriByNumero(TelephoneNumber));
                Android.App.Application.Context.StartActivity(aInt1);
            }
            else
            {
                if (alarm.missed_calls != 0)
                {
                    //ToastColor.MakeToastColorq(alarm.missed_calls, "Attenzione, negli ultimi 5 minuti ti ha chiamato " + nome + " "  +alarm.missed_calls + " volta\\e", context);
                }
                else if (alarm.missed_calls == 0)
                {
                    //sqlite.Clear_Call();
                }
            }
        }
    }
}