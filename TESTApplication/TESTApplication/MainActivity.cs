﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android.Content;
using System.Collections.Generic;
using Android.Provider;
using TESTApplication.PersonalDataQueries;
using Android.Views;
using Android;
using System;
using Android.Support.V4.App;
using Android.Content.PM;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;

using Android.Net;
using static Android.Provider.ContactsContract.CommonDataKinds;
using TESTApplication.Utility;
using Android.Graphics;
using Refractored.Controls;
using System.Linq;

namespace TESTApplication
{
   
    public class MainActivity : AppCompatActivity
    {
        bool write = false;
        string uri_str = "";
        //View layout;
        CustomListAdapter custom;
        ImageButton floatingActionButton;

        List<PriorityNumbers> priorityNumbers;
        ListView contacts;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            
            base.OnCreate(savedInstanceState);
            
            //layout = this.FindViewById(Android.Resource.Id.Content);
            SetContentView(Resource.Layout.Main);

            //var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);

            //toolbar.SetTitleTextColor(Color.White);
            //SetSupportActionBar(toolbar);
            
            Check_Permissions();
            floatingActionButton = FindViewById<ImageButton>(Resource.Id.fab);
            floatingActionButton.Click += Btn_ClicCercaContatto;

            contacts = FindViewById<ListView>(Resource.Id.ListView);
            contacts.ItemClick += Contacts_ItemClick;
            contacts.ItemLongClick += listView_ItemLongClick;
        }

        //public override bool OnCreateOptionsMenu(IMenu menu)
        //{
        //    //MenuInflater.Inflate(Resource.Menu.top_menus, menu);
        //    return base.OnCreateOptionsMenu(menu);
        //}
        //public override bool OnOptionsItemSelected(IMenuItem item)
        //{
        //    Toast.MakeText(this, "Action selected: " + item.TitleFormatted,
        //        ToastLength.Short).Show();
        //    return base.OnOptionsItemSelected(item);
        //}

        Dialog dialog;

        string contact_to_delete = "-1";
        private void listView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {

            dialog = new Dialog(this);
            dialog.RequestWindowFeature((int)WindowFeatures.NoTitle); //before  
            dialog.SetContentView(Resource.Layout.custom_dialog);
            ListView list = (ListView)sender;
            var item = list.GetItemAtPosition(e.Position);
            PriorityNumbers test = ObjectTypeHelper.Cast<PriorityNumbers>(item);


            ImageButton btnDelete = (ImageButton)dialog.FindViewById(Resource.Id.save);


            CircleImageView image = (CircleImageView)dialog.FindViewById(Resource.Id.imageViewContact);
            TextView utente = (TextView)dialog.FindViewById(Resource.Id.editText);


            image.SetImageURI(Android.Net.Uri.Parse(test.photo_uri));

            if (image.Drawable == null)
            {
                image.SetImageResource(Resource.Drawable.ContactImage);
            }

            utente.Text = test.Descrizione;
            btnDelete.Click += BtnDelete_Click;
            contact_to_delete = test.Contact_ID;

            dialog.Show();

          

        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            Sqlite sqlite = new Sqlite();
            sqlite.Delete_PNumbers(contact_to_delete);
            Get_Number();
            dialog.Dismiss();
        }

        private void Contacts_LongClick(object sender, View.LongClickEventArgs e)
        {

        }

        public View getViewByPosition(int pos, ListView listView)
        {
            int firstListItemPosition = listView.FirstVisiblePosition;
            int lastListItemPosition = firstListItemPosition + listView.ChildCount - 1;

            if (pos < firstListItemPosition || pos > lastListItemPosition)
            {
                return listView.Adapter.GetView(pos, null, listView);
            }
            else
            {
                int childIndex = pos - firstListItemPosition;
                return listView.GetChildAt(childIndex);
            }
        }


        private void Contacts_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ListView list = (ListView)sender;
            var item = list.GetItemAtPosition(e.Position);
            View v = getViewByPosition(e.Position, list);

            var text_1 = v.FindViewById<TextView>(Resource.Id.Title).Text;

            PriorityNumbers test = ObjectTypeHelper.Cast<PriorityNumbers>(item);
            Sqlite sqlite = new Sqlite();
            sqlite.Insert_VIC_Level(test.Contact_ID, VicConverter.Next_Vic_Code(test.VicLevel));
            //

            test.VicLevel = VicConverter.Next_Vic_Code(test.VicLevel);


            if (v == null)
                return;

            VicConverter.VicItemListViewConverter(v, test);

        }

        private void Btn_ClickInserisciContatto(object sender, EventArgs e)
        {

            Intent intent = new Intent(Intent.ActionInsert);
            intent.SetType(ContactsContract.Contacts.ContentType);
            StartActivityForResult(intent, 18);
        }
        private void Btn_ClicCercaContatto(object sender, EventArgs e)
        {
            //StartActivity(typeof(ContactActivity));

            Intent intent = new Intent(Intent.ActionPick);
            intent.SetData(ContactsContract.Contacts.ContentUri);
            StartActivityForResult(intent, 17);
        }


        protected override void OnResume()
        {
            base.OnResume();

            if (write)
                Get_Number();
        }

        private bool ValidatePhoneNumber(string phone)
        {
            return Android.Util.Patterns.Phone.Matcher(phone).Matches();
        }

        private void Get_Number()
        {
            ////edtNumeroPrioritario = FindViewById<TextView>(Resource.Id.edtNumeroPrioritario);
            //Sqlite sqlite = new Sqlite();
            ////edtNumeroPrioritario.Text = sqlite.Get_Number();
            //image.SetImageURI(Android.Net.Uri.Parse(sqlite.Get_PhotoUri()));
            //if (image.Drawable == null)
            //{
            //    image.SetImageResource(Resource.Drawable.ContactImage);
            //}
            //descrizione.Text = sqlite.Get_Descrizione();

            RefreshGrid();
        }

        private void RefreshGrid()
        {

            Sqlite sqlite = new Sqlite();
            priorityNumbers = new List<PriorityNumbers>();
            priorityNumbers = sqlite.Get_Contacts();
            // populate the listview with data

            custom = new CustomListAdapter(this, priorityNumbers);
            contacts.Adapter = custom;

            ListView listViewFriends = (ListView)FindViewById(Resource.Id.ListView);
            // set your adapter here
            // set your click listener here
            // or whatever else
            listViewFriends.EmptyView = FindViewById(Resource.Id.empty);
        }

        private void Btn_Click(object sender, EventArgs e)
        {


            //if (!ValidatePhoneNumber(edtNumeroPrioritario.Text))
            //{
            //    //Snackbar.Make(layout, "Errore inserimento. Controlla il numero inserito", Snackbar.LengthLong).Show();
            //    return;
            //}
            //Sqlite sqlite = new Sqlite();
            //string old = sqlite.Get_Number();
            //Insert_Number();
            //if (old != edtNumeroPrioritario.Text)
            //Snackbar.Make(layout, "Numero modificato. Passa alla versione PRO per inserire più numeri contemporaneamente!!!", Snackbar.LengthIndefinite).SetAction("OK", new Action<View>(delegate (View obj) { })).Show();
        }

        public void Check_Permissions()
        {
            var requiredPermissions = new String[] {
                Manifest.Permission.ReadExternalStorage,
                Manifest.Permission.WriteExternalStorage,
                Manifest.Permission.ReadPhoneState,
                Manifest.Permission.AccessNotificationPolicy,
                Manifest.Permission.CallPhone,
                Manifest.Permission.Vibrate,
                Manifest.Permission.ReadContacts,
                Manifest.Permission.WriteContacts,
                Manifest.Permission.ReceiveBootCompleted
                //claudio
                ,
                //Manifest.Permission.ReadSms,
                //Manifest.Permission.ReceiveSms ,
                Manifest.Permission.ReadCallLog 
                //claudio
               };
            ActivityCompat.RequestPermissions(this, requiredPermissions, 1);
        }

        //private void Check_DOD()
        //{
        //    if (Build.VERSION.SdkInt >= Build.VERSION_CODES.M)
        //    {
        //        if (!mNotificationManager.IsNotificationPolicyAccessGranted)
        //        {
        //            Intent intent = new Intent(Settings.ActionNotificationPolicyAccessSettings);
        //            StartActivity(intent);
        //        }
        //    }
        //}



        //public void Check_ReadExternalStorage()
        //{  var requiredPermissions = new String[] { Manifest.Permission.ReadExternalStorage };
        //    if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
        //    {
        //        Snackbar.Make(layout, "Nessun diritto di accesso memoria in lettura. Vuoi Autorizzare?", Snackbar.LengthIndefinite).SetAction("OK", new Action<View>(delegate (View obj) { ActivityCompat.RequestPermissions(this, requiredPermissions, 1); })).Show();
        //    }
        //    else
        //    {
        //        Get_Number();
        //    }
        //}
        //public void Check_WriteExternalStorage()
        //{
        //    var requiredPermissions = new String[] { Manifest.Permission.WriteExternalStorage };
        //    if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
        //    {
        //        Snackbar.Make(layout, "Nessun diritto di accesso memoria in scrittura. Vuoi Autorizzare?", Snackbar.LengthIndefinite).SetAction("OK", new Action<View>(delegate (View obj) { ActivityCompat.RequestPermissions(this, requiredPermissions, 2); })).Show();
        //    }
        //    else
        //    {
        //        Insert_Number();
        //    }
        //}
        //public void Check_ReadPhoneState()
        //{
        //    var requiredPermissions = new String[] { Manifest.Permission.ReadPhoneState };
        //    if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadPhoneState) != (int)Permission.Granted)
        //    {
        //        Snackbar.Make(layout, "Nessun diritto di lettura stato telefono. Vuoi Autorizzare?", Snackbar.LengthIndefinite).SetAction("OK", new Action<View>(delegate (View obj) { ActivityCompat.RequestPermissions(this, requiredPermissions, 3); })).Show();
        //    }
        //}

        //            <uses-permission android:name="android.permission.WRITE_SETTINGS"/>
        //<uses-permission android:name="android.permission.WRITE_SECURE_SETTINGS"/>

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            if (requestCode == 1)
            {
                // Scrittura
                if (grantResults[0] == Permission.Granted)
                {
                    write = true;
                    Get_Number();
                }

                // Lettura
                if (grantResults[1] == Permission.Granted)
                {

                }

                // Stato Telefono
                if (grantResults[2] == Permission.Granted)
                {

                }
                if (grantResults[3] == Permission.Granted)
                {

                }
                if (grantResults[4] == Permission.Granted)
                {

                }
                if (grantResults[5] == Permission.Granted)
                {

                }
                if (grantResults[6] == Permission.Granted)
                {

                }
                if (grantResults[7] == Permission.Granted)
                {

                }
                if (grantResults[8] == Permission.Granted)
                {

                } //claudio
                if (grantResults[9] == Permission.Granted)
                {

                }
            

              
            }

            else
            {
                base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            }


        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case (17):

                    string numero = "";
                    int count_numeri = 0;
                    if (resultCode == Result.Ok)
                    {

                        Android.Net.Uri contactData = data.Data;
                        var c = ContentResolver.Query(contactData, null, null, null, null);
                        if (c.MoveToFirst())
                        {

                            String contactId = c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.Id));
                            long contactId_long = long.Parse(c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.Id)));

                            var contactUri = ContentUris.WithAppendedId(ContactsContract.Contacts.ContentUri, contactId_long);
                            var contactPhotoUri = Android.Net.Uri.WithAppendedPath(contactUri, Contacts.Photos.ContentDirectory);
                            
                            var uri = ContactsContract.Contacts.ContentUri;
                            uri_str = contactPhotoUri.ToString();
                            string[] projection = { Phone.Number, CommonColumns.Type };

                            var cursor = ContentResolver.Query(Phone.ContentUri, projection, ContactsContract.RawContactsColumns.ContactId + "=" + contactId, null, null);
                            count_numeri = cursor.Count;
                            if (cursor != null)
                            {
                                if (count_numeri > 0)
                                {
                                    while (cursor.MoveToNext())
                                    {
                                        Sqlite sqlite = new Sqlite();
                                        string num  = cursor.GetString(0);
                                        string result = string.Concat(num.Where(rr => !char.IsWhiteSpace(rr)));
                                       
                                        sqlite.Insert_PNumbers(result, uri_str, c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.DisplayName)), c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.Id)), 2);
                                    }
                                }
                            }
                            c.Close();
                            cursor.Close();
                        }
                    }
                    Get_Number();
                    break;
            }

        }

    }

    public static class ObjectTypeHelper
    {
        public static T Cast<T>(this Java.Lang.Object obj) where T : class
        {
            var propertyInfo = obj.GetType().GetProperty("Instance");
            return propertyInfo == null ? null : propertyInfo.GetValue(obj, null) as T;
        }
    }
}


