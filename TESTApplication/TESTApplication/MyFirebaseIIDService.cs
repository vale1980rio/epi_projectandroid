﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
using Android.App;
using Android.Content;
 
using Firebase.Iid;
using Android.Util;


namespace TESTApplication
{

    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" }, Priority = (int)IntentFilterPriority.HighPriority)]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "MyFirebaseIIDService";
        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(TAG, "Refreshed token: " + refreshedToken);
            SendRegistrationToServer(refreshedToken);
        }
        void SendRegistrationToServer(string token)
        {
            // Add custom implementation, as needed.
        }
    }
}