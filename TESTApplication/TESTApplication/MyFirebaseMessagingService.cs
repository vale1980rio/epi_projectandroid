﻿
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Support.V4.App;

using Firebase.Messaging;
using TESTApplication.Utility;

namespace TESTApplication
{


    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        Sqlite sqlite = new Sqlite();
    
        Vibrator vibrator = (Vibrator)Application.Context.GetSystemService(Context.VibratorService);


        static readonly string TAG = "StartActivity";

        internal static readonly string CHANNEL_ID = "my_notification_channel";
        internal static readonly int NOTIFICATION_ID = 100;


        public override void OnMessageReceived(RemoteMessage message)
        {

            SendNotification(message.Data);
        }

        void SendNotification(IDictionary<string, string> data)
        {

           
            var intent = new Intent(this, typeof(StartActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            foreach (var key in data.Keys)
            {
                intent.PutExtra(key, data[key]);
            }

           

            var pendingIntent = PendingIntent.GetActivity(this,
                                                          NOTIFICATION_ID,
                                                          intent,
                                                          PendingIntentFlags.OneShot);

            var notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                                      .SetSmallIcon(Resource.Drawable.alarm_mini_red)
                                      .SetContentTitle("CANALE: "+ data["canale"].Replace("_", " ") + " - " + data["title"])
                                      .SetContentText(data["body"])
                                      .SetAutoCancel(false)

                                      .SetContentIntent(pendingIntent);

            var notificationManager = NotificationManagerCompat.From(this);

            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());

            sqlite.Insert_Message(data["canale"].Replace("_", " "), data["title"], data["body"]);
            startAlarm(data["canale"].Replace("_", " "), data["title"], data["body"]);


        }

        private void startAlarm(string topic,string titolo, string messaggio)
        {   //UCCIDE IL POPUP PRECEDENTE SE ATTIVO.
            Intent i = new Intent("android.intent.action.DIALOG_FCM");
            i.PutExtra("titolo", titolo);
            i.PutExtra("messaggio", messaggio);
            i.PutExtra("topic", topic);
            ApplicationContext.SendBroadcast(i);


            Intent aInt1 = new Intent();
            aInt1.AddFlags(ActivityFlags.SingleTop);
            aInt1.AddFlags(ActivityFlags.NewTask);
            aInt1.AddFlags(ActivityFlags.NoHistory);
            aInt1.SetClass(Android.App.Application.Context, typeof(DialogActivityFCM));


            aInt1.PutExtra("topic", topic);
            aInt1.PutExtra("titolo", titolo);
            aInt1.PutExtra("messaggio", messaggio);

            Android.App.Application.Context.StartActivity(aInt1);

        }
    }
}
