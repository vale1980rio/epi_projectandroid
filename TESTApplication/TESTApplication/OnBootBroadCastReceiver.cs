﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TESTApplication.Utility;

namespace TESTApplication
{

    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted })]
    public class BootReceiver : BroadcastReceiver
    {

        public override void OnReceive(Context context, Intent intent)
        {
            ToastColor.YellowToastColor("Boot Completed!!! VIC is running!! ", context);
        }
    }
}