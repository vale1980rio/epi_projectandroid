﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace TESTApplication.PersonalDataQueries
{
    [Table("Calls")] 
    public class Call
    {
        public string Number { get; set; }
        public int CallType { get; set; }
        public string CallType_str { get; set; }
        public DateTime Date{ get; set; }

        public string Date_str { get; set; }
    }
}


