﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Icu.Text;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Widget;

namespace TESTApplication.PersonalDataQueries
{
    public class PersonalInformations
    {
        //Numero chiamate ad un numero in un range temporale prima che suoni l'allarme
        private int _chiamate = 5;
        //Tutte le chiamate
        private List<Call> calls;
        //Tutte le chiamate fatte negli ultimi x minuti (valore configurabile default 5 minuti)
        private int _minuti = 5;
        public int  missed_calls { get; set; }
        private List<Call> range;

        //Tutte le chiamate relative ad un numero nel range di tempo selezionato
        private List<Call> calls_number;

        #region METODI PUBBLICI E COSTRUTTORI
        public PersonalInformations(int minuti, int chiamate, string number)
        {
            _minuti = minuti;
            _chiamate = chiamate;
            GET_ALL_CALLS();
            CALLS_LAST_X_MINUTES(_minuti);
            CALLS_BY_NUMBERS(number);
            MISSED_CALLS();
        }

        public void Refresh(int minuti, int chiamate, string number)
        {
            _minuti = minuti;
            _chiamate = chiamate;
            GET_ALL_CALLS();
            CALLS_LAST_X_MINUTES(_minuti);
            CALLS_BY_NUMBERS(number);
            MISSED_CALLS();
        }

        #endregion

        #region UTILITY
        private static string CallType_Translator(int type)
        {
            string type_str = "";

            switch (type)
            {
                case (int)CallType.AnsweredExternally:
                    type_str = "AnsweredExternally";
                    break;
                case (int)CallType.Blocked:
                    type_str = "Blocked";
                    break;
                case (int)CallType.Incoming:
                    type_str = "Incoming";
                    break;
                case (int)CallType.Missed:
                    type_str = "Missed";
                    break;
                case (int)CallType.Outgoing:
                    type_str = "Outgoing";
                    break;
                case (int)CallType.Rejected:
                    type_str = "Rejected";
                    break;
                case (int)CallType.Voicemail:
                    type_str = "Voicemail";
                    break;
            }
            return type_str;
        }
        //Converte il TimeStamp Unix Proveniente dalle Log in un DateTime
        private static DateTime Convert_TIMESTAMP_DATETIME(double unixTimeStamp)
        {
            double seconds = unixTimeStamp / 1000;
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(seconds).ToLocalTime();
            return dtDateTime;
        }
        #endregion

        #region QUERY
        //Legge la history delle chiamate
        private List<Call> GET_ALL_CALLS()
        {
            Sqlite sqlite = new Sqlite();
            calls = sqlite.Get_All_Calls();
            return calls;
          
        }
        //Prendo tutte le chiamate fatte negli ultimi x minuti
        private void CALLS_LAST_X_MINUTES(int minuti)
        {
            int reverse = -minuti;

            DateTime back = DateTime.Now.AddMinutes(reverse).AddHours(-2);
            range = calls.Where(p => DateTime.Parse(p.Date_str) > back).ToList<Call>();
        }
        
        //Prendo tutte le chiamate relative ad un numero
        private void CALLS_BY_NUMBERS(string number)
        {
            calls_number = range.Where(p => p.Number.Contains(number)).ToList<Call>();
        }
        #endregion


        private void MISSED_CALLS()
        {
            DateTime confine = DateTime.Now.AddYears(-10);
            //Prendo tutte le chiamate in uscita ed in entrata fatte nel range temporale
            List<Call> Out_In = new List<Call>();
            try
            {
                Out_In = calls_number.Where(p => ((p.CallType_str == "Offhook"))).OrderBy(p => p.Date).ToList<Call>();

                if (Out_In.Count > 0)
                {
                    string conf_str = Out_In.Last().Date_str;

                    confine = DateTime.Parse(conf_str);
               
                }
            }

            catch { }

            //Prendo Tutte le chiamate perse e rifiutate dopo l'ultima chiamata  sopra presa in considerazione.

           missed_calls = calls_number.Where(p => (p.CallType_str == "Ringing") && DateTime.Parse(p.Date_str) >= confine).Count();
        }
        

        public bool CHECK_STATUS_MISSED_CALLS()
        {
            if (missed_calls >= _chiamate)
                return true;
            return false;
        }






        //1° Controllo quante chiamate sono state fatte

    }
}