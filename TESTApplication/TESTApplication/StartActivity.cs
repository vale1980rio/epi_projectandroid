﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Common;
using Android.OS;
using Android.Runtime;

using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Firebase.Messaging;
using TESTApplication.Fragments;

namespace TESTApplication
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class StartActivity : AppCompatActivity, Android.Support.Design.Widget.NavigationView.IOnNavigationItemSelectedListener
    {
        Fragment_Home newFragment_home = null;
        Fragment_Contatti newFragment = null;
        Fragment_Canali newFragment_canali = null;
        Fragment_Canali_Aggiungi newFragment_canali_aggiungi = null;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            FirebaseMessaging.Instance.SubscribeToTopic("TEST");
            FirebaseMessaging.Instance.SubscribeToTopic("ROMA_EUR");
            FirebaseMessaging.Instance.SubscribeToTopic("GENERALE");
            FirebaseMessaging.Instance.SubscribeToTopic("PROTEZIONE_CIVILE");

            SetContentView(Resource.Layout.activity_main);

            var test = this.Intent.GetStringExtra("canale");


            if (test != null)
            {
                EdificiFragment();
            }
            else
            {

                HomeFragment();
            }
            #region Menu
            //Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);



            //Android.Support.V4.Widget.DrawerLayout drawer = FindViewById<Android.Support.V4.Widget.DrawerLayout>(Resource.Id.drawer_layout);
            //ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            //drawer.AddDrawerListener(toggle);
            //toggle.SyncState();

            Android.Support.Design.Widget.NavigationView navigationView = FindViewById<Android.Support.Design.Widget.NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);
            #endregion
            IsPlayServicesAvailable();

            CreateNotificationChannel();








            // Create your application here


        }

        static readonly string TAG = "StartActivity";

        internal static readonly string CHANNEL_ID = "my_notification_channel";
        internal static readonly int NOTIFICATION_ID = 100;

        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channel = new NotificationChannel(CHANNEL_ID,
                                                  "FCM Notifications",
                                                  NotificationImportance.Default)
            {

                Description = "Firebase Cloud Messages appear in this channel"
            };


            var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }
        private void HomeFragment()
        {
            newFragment_home = new Fragment_Home();
            FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
            fragmentTx.Replace(Resource.Id.frameLayout1, newFragment_home);
            fragmentTx.AddToBackStack(null);
            fragmentTx.Commit();
        }

        private void EdificiFragment()
        {
            newFragment_canali = new Fragment_Canali();
            FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
            fragmentTx.Replace(Resource.Id.frameLayout1, newFragment_canali);
            fragmentTx.AddToBackStack(null);
            fragmentTx.Commit();
        }
        #region Controllo Permission
        public void Check_Permissions()
        {
            var requiredPermissions = new String[] {
               Android.Manifest.Permission.ReadExternalStorage,
               Android.Manifest.Permission.WriteExternalStorage,
               Android.Manifest.Permission.ReadPhoneState,
               Android.Manifest.Permission.AccessNotificationPolicy,
               Android.Manifest.Permission.CallPhone,
               Android.Manifest.Permission.Vibrate,
               Android.Manifest.Permission.ReadContacts,
               Android.Manifest.Permission.WriteContacts,
               Android.Manifest.Permission.ReceiveBootCompleted
                //claudio
                ,
                //Manifest.Permission.ReadSms,
                //Manifest.Permission.ReceiveSms ,
                 Android.Manifest.Permission.ReadCallLog 
                //claudio
               };
            Android.Support.V4.App.ActivityCompat.RequestPermissions(this, requiredPermissions, 1);
        }
        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {

                }
                //msgText.Text = GoogleApiAvailability.Instance.GetErrorString(resultCode);
                else
                {

                    //msgText.Text = "This device is not supported";
                    Finish();
                }
                return false;
            }
            else
            {

                //msgText.Text = "Google Play Services is available.";
                return true;
            }
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            if (requestCode == 1)
            {
                // Scrittura


                if (grantResults[0] == Permission.Granted)
                {
                    if (newFragment != null)
                    {
                        newFragment.write = true;
                        newFragment.Get_Number();
                    }
                }

                // Lettura
                if (grantResults[1] == Permission.Granted)
                {

                }

                // Stato Telefono
                if (grantResults[2] == Permission.Granted)
                {

                }
                if (grantResults[3] == Permission.Granted)
                {

                }
                if (grantResults[4] == Permission.Granted)
                {

                }
                if (grantResults[5] == Permission.Granted)
                {

                }
                if (grantResults[6] == Permission.Granted)
                {

                }
                if (grantResults[7] == Permission.Granted)
                {

                }
                if (grantResults[8] == Permission.Granted)
                {

                } //claudio
                if (grantResults[9] == Permission.Granted)
                {

                }



            }

            else
            {
                base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            }


        }
        #endregion

        protected override void OnResume()
        {
            base.OnResume();

        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            int id = item.ItemId;


            if (id == Resource.Id.nav_contatti)
            {
                newFragment = new Fragment_Contatti();
                FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
                fragmentTx.Replace(Resource.Id.frameLayout1, newFragment);
                fragmentTx.AddToBackStack(null);
                fragmentTx.Commit();
            }

            else if (id == Resource.Id.nav_allarmi_edifici)
            {
                newFragment_canali = new Fragment_Canali();
                FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
                fragmentTx.Replace(Resource.Id.frameLayout1, newFragment_canali);
                fragmentTx.AddToBackStack(null);
                fragmentTx.Commit();
            }

            else if (id == Resource.Id.nav_canale)
            {
                newFragment_canali_aggiungi = new Fragment_Canali_Aggiungi();
                FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
                fragmentTx.Replace(Resource.Id.frameLayout1, newFragment_canali_aggiungi);
                fragmentTx.AddToBackStack(null);
                fragmentTx.Commit();
            }

            else if (id == Resource.Id.nav_home)
            {
                HomeFragment();
            }

            Android.Support.V4.Widget.DrawerLayout drawer = FindViewById<Android.Support.V4.Widget.DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);

            return true;
        }
        public override void OnBackPressed()
        {
            Android.Support.V4.Widget.DrawerLayout drawer = FindViewById<Android.Support.V4.Widget.DrawerLayout>(Resource.Id.drawer_layout);
            if (drawer.IsDrawerOpen(Android.Support.V4.View.GravityCompat.Start))
            {
                drawer.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {


                return true;
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}