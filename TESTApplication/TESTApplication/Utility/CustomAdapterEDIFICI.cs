﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Refractored.Controls;


namespace TESTApplication.Utility
{
    public class CustomAdapterEDIFICI : BaseAdapter<string>
    {
        Activity context;
        List<string> list;

        public CustomAdapterEDIFICI(Activity _context, List<string> _list)
            : base()
        {
            this.context = _context; 
            this.list = _list;
        }

        public override int Count
        {
            get { if (list != null) return list.Count; else return 0; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override string this[int index]
        {
            get { return list[index]; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;


            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItemRowEDIFICI, parent, false);

            string item = this[position];
            
            view.FindViewById<TextView>(Resource.Id.Description).Text = item;
            switch (item)
            {
                case "ROMA EUR":
                    {
                        CircleImageView ci = view.FindViewById<CircleImageView>(Resource.Id.Thumbnail);
                        ci.Visibility = ViewStates.Visible;
                        break;
                    }
                case "GENERALE":
                    {
                        CircleImageView ci = view.FindViewById<CircleImageView>(Resource.Id.Thumbnail_GE);
                        ci.Visibility = ViewStates.Visible;
                        break;
                    }
                case "PROTEZIONE CIVILE":
                    {
                        CircleImageView ci = view.FindViewById<CircleImageView>(Resource.Id.Thumbnail_PC);
                        ci.Visibility = ViewStates.Visible;
                        break;
                    }
            }

            return view;
        }

    }
}