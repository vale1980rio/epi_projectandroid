﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Refractored.Controls;


namespace TESTApplication.Utility
{
    public class CustomAdapterFCM : BaseAdapter<Alert_Messages>
    {
        Activity context;
        List<Alert_Messages> list;

        public CustomAdapterFCM(Activity _context, List<Alert_Messages> _list)
            : base()
        {
            this.context = _context;
            this.list = _list;
        }

        public override int Count
        {
            get { if (list != null) return list.Count; else return 0; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Alert_Messages this[int index]
        {
            get { return list[index]; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;


            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItemRowFCM, parent, false);

            Alert_Messages item = this[position];
            view.FindViewById<TextView>(Resource.Id.Title).Text = item.Titolo;
            view.FindViewById<TextView>(Resource.Id.Description).Text = item.Messaggio;
            view.FindViewById<TextView>(Resource.Id.Data).Text = item.Data;
            view.FindViewById<TextView>(Resource.Id.Topic).Text ="CANALE: "+ item.Canale;
            return view;
        }

    }
}