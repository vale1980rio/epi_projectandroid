﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Text.Style;
using Android.Views;
using Android.Widget;

namespace TESTApplication.Utility
{
    public static class ToastColor
    {
        public static void MakeToastColor(int missed, string message, Context context)
        {
            RedToastColor(message, context);
        }

        public static void RedToastColor(string message, Context context)
        {
            SpannableString span = new SpannableString(message);
            span.SetSpan(new ForegroundColorSpan(Color.White), 0, message.Length, SpanTypes.ExclusiveExclusive);

            var toast = Toast.MakeText(context, span, ToastLength.Long);
            View ToastView = toast.View;
            TextView v = (TextView)ToastView.FindViewById(Android.Resource.Id.Message);
            LinearLayout ll = (LinearLayout)ToastView.FindViewById(Android.Resource.Id.Message).Parent;
            ll.SetBackgroundResource(Resource.Drawable.custom_toast_red);
            v.Gravity = GravityFlags.Center;
            //ToastView.SetBackgroundColor(Color.ParseColor("#dc3545"));
            toast.SetGravity(GravityFlags.Center, 0, 0);
            toast.Show();
        }

        public static void YellowToastColor(string message, Context context)
        {
            SpannableString span = new SpannableString(message);
            span.SetSpan(new ForegroundColorSpan(Color.Black), 0, message.Length, SpanTypes.ExclusiveExclusive);

            var toast = Toast.MakeText(context, span, ToastLength.Long);
            View ToastView = toast.View;
            TextView v = (TextView)ToastView.FindViewById(Android.Resource.Id.Message);
            LinearLayout ll = (LinearLayout)ToastView.FindViewById(Android.Resource.Id.Message).Parent;
            ll.SetBackgroundResource(Resource.Drawable.custom_toast_yellow);
            v.Gravity = GravityFlags.Center;
            //ToastView.SetBackgroundColor(Color.ParseColor("#ffc107"));
            toast.SetGravity(GravityFlags.Center, 0, 0);
            toast.Show();
        }


        public static void PosteToastColor(string message, Context context)
        {
            SpannableString span = new SpannableString(message);
            span.SetSpan(new ForegroundColorSpan(Color.Black), 0, message.Length, SpanTypes.ExclusiveExclusive);

            var toast = Toast.MakeText(context, span, ToastLength.Long);
            View ToastView = toast.View;
            TextView v = (TextView)ToastView.FindViewById(Android.Resource.Id.Message);
            v.SetTextColor(Color.White);
            LinearLayout ll = (LinearLayout)ToastView.FindViewById(Android.Resource.Id.Message).Parent;
            ll.SetBackgroundResource(Resource.Drawable.custom_toast_poste);
            v.Gravity = GravityFlags.Center;
            //ToastView.SetBackgroundColor(Color.ParseColor("#ffc107"));
            toast.SetGravity(GravityFlags.Center, 0, 0);
            toast.Show();
        }

        public static void GreenToastColor(string message, Context context)
        {
            SpannableString span = new SpannableString(message);
            span.SetSpan(new ForegroundColorSpan(Color.White), 0, message.Length, SpanTypes.ExclusiveExclusive);
            var toast = Toast.MakeText(context, span, ToastLength.Long);
            View ToastView = toast.View;
            TextView v = (TextView)ToastView.FindViewById(Android.Resource.Id.Message);
            LinearLayout ll = (LinearLayout)ToastView.FindViewById(Android.Resource.Id.Message).Parent;
            ll.SetBackgroundResource(Resource.Drawable.custom_toast_green);
            v.Gravity = GravityFlags.Center;
            //ToastView.SetBackgroundColor(Color.ParseColor("#28a745"));
            toast.SetGravity(GravityFlags.Center, 0, 0);
            toast.Show();
        }
    }
}